# PM10 on New Years
This short repo holds data and visualization for showing the PM10 ("Feinstaub") around New Year.

![built result plot](https://codeberg.org/attachments/f45f2cac-6178-44f1-b59e-172a091a047a)

## Data Source
[Sächsisches Staatsministerium für Umwelt und Landwirtschaft](https://www.umwelt.sachsen.de/umwelt/infosysteme/luftonline/Recherche.aspx)

These plots use `PM10_THEOM` (automatically determined).
Note that actual legal limits are based on `PM10_HVS`, which is determined in a lab, but is only available after some delay.

## Build
You need R and ggplot2.
With `pm10.csv` visible in your pwd run:

```
R --vanilla < ./plot.R
```

## FAQ
- Why not PM2.5?
  - Because the data is not available hourly.
- What conclusions can be drawn from the data?
  - They do *not* cross the legal limits. For anything else ask somebody more knowledgeable.
- What is the legal limit?
  - see [here, table on the right](https://en.wikipedia.org/wiki/Particulates#European_Union) TL;DR 40 µg/m³ yearly mean
